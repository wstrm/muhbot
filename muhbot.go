package main

import (
	"crypto/tls"
	"log"
	"math/rand"
	"time"

	"gitlab.com/willeponken/muhbot/service"

	"github.com/thoj/go-ircevent"
)

type muhbot struct {
	nick        string
	user        string
	owner       string
	address     string
	password    string
	channels    []string
	tls         bool
	tlsNoVerify bool
	debug       bool
	irc         *irc.Connection
}

func (bot *muhbot) connect() error {
	i := irc.IRC(bot.nick, bot.user)
	i.UseTLS = bot.tls
	i.Password = bot.password
	i.Debug = bot.debug

	if bot.tls {
		i.TLSConfig = new(tls.Config)
		i.TLSConfig.InsecureSkipVerify = bot.tlsNoVerify
	}

	if err := i.Connect(bot.address); err != nil {
		return err
	}

	i.AddCallback("001", func(event *irc.Event) {
		for _, channel := range bot.channels {
			i.Join(channel)
		}
	})

	bot.irc = i

	return nil
}

func main() {
	var err error

	rand.Seed(time.Now().UnixNano())

	bot := muhbot{
		address:     context.address,
		nick:        context.nick,
		user:        context.user,
		owner:       context.owner,
		password:    context.password,
		channels:    context.channels.List(),
		tls:         context.tls,
		tlsNoVerify: context.tlsNoVerify,
		debug:       context.debug,
	}

	err = bot.connect()
	if err != nil {
		log.Fatal(err)
	}

	irc := bot.irc

	aj := new(service.AutoJoin)
	aj.Setup(irc, bot.nick)

	gmp := new(service.GoodMorning)
	gmp.SetTime("07:00")
	gmp.SetRandom(2, 32)
	gmp.SetChannels([]string{"#htsit"})
	gmp.Setup(irc)

	logger := new(service.Logger)
	logger.SetChannels([]string{"#htsit", "#krmChat"})
	logger.SetDir(context.logDir)
	logger.SetKeepTime(time.Duration((7 * 24)) * time.Hour) // 7 days
	logger.Setup(irc)

	help := new(service.Help)
	help.Setup(irc, bot.nick, aj, help)

	irc.Loop()
}
