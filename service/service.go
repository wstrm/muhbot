package service

import "strings"

type ircService interface {
	Usage() string
}

func sanitizeString(str string) string {
	return strings.ToLower(strings.TrimSpace(str))
}

func checkCommand(command string, name string, a []string) (args []string, hl bool) {
	channel := sanitizeString(a[0])
	evArgs := strings.Split(sanitizeString(a[1]), " ")

	if channel == name {
		hl = true
		args = evArgs[:len(evArgs)-1]
	}

	if len(evArgs) >= 2 {
		highlight := strings.TrimSuffix(sanitizeString(evArgs[0]), ":") // Trim ':' from ex. "muhbot: ..."
		cmd := sanitizeString(evArgs[1])

		if highlight == name && command == cmd {
			hl = true
		}

		if len(evArgs) > 3 {
			args = evArgs[:len(evArgs)-1]
		}
	}

	return
}
