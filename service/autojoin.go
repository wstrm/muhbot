package service

import (
	"fmt"

	"github.com/thoj/go-ircevent"
)

// AutoJoin ...
type AutoJoin struct {
	ircService
	command, usage string
}

// Setup ...
func (a *AutoJoin) Setup(conn *irc.Connection, botNick string) error {
	a.usage = fmt.Sprintf("'/INVITE %s': invites me into the channel", botNick)

	conn.AddCallback("INVITE", a.eventListener)

	return nil
}

// Usage ...
func (a *AutoJoin) Usage() string {
	return a.usage
}

func (a *AutoJoin) eventListener(event *irc.Event) {
	go func(event *irc.Event) {
		conn := event.Connection
		channel := event.Message()
		conn.Join(channel)
	}(event)
}
