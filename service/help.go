package service

import (
	"fmt"

	"github.com/thoj/go-ircevent"
)

// Help ...
type Help struct {
	ircService
	helpStrings             []string
	usage, command, botNick string
}

func (h *Help) eventListener(event *irc.Event) {
	go func(event *irc.Event) {
		conn := event.Connection

		_, ok := checkCommand(h.command, h.botNick, event.Arguments)
		if ok && event.Nick != h.botNick {
			for _, str := range h.helpStrings {
				conn.Privmsg(event.Nick, str)
			}
		}
	}(event)
}

// Setup ...
func (h *Help) Setup(conn *irc.Connection, botNick string, services ...ircService) error {
	h.helpStrings = append(h.helpStrings, fmt.Sprintf("Hello, I'm %s. I can do plenty of things:\n", botNick))

	for _, s := range services {
		usage := s.Usage()
		if usage != "" {
			h.helpStrings = append(h.helpStrings, fmt.Sprintf("%s\n", usage))
		}
	}

	h.command = "help"
	h.usage = "Prints a help message"
	h.botNick = botNick

	h.helpStrings = append(h.helpStrings, fmt.Sprintf("%s: %s\n", h.command, h.usage))

	conn.AddCallback("PRIVMSG", h.eventListener)

	return nil
}

// Usage ...
func (h *Help) Usage() string {
	return fmt.Sprintf("%s: %s", h.command, h.usage)
}
