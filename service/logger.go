package service

import (
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/thoj/go-ircevent"
)

// Logger ...
type Logger struct {
	ircService
	logChannels []string
	logDir      string
	logKeepTime time.Duration
}

// SetChannels ...
func (l *Logger) SetChannels(channels []string) {
	l.logChannels = channels
}

// SetDir ...
func (l *Logger) SetDir(dir string) {
	l.logDir = dir
}

// SetKeepTime ...
func (l *Logger) SetKeepTime(keepTime time.Duration) {
	l.logKeepTime = keepTime
}

func (l *Logger) fromLogChannel(from string) (isLogChannel bool) {
	for _, channel := range l.logChannels {
		if from == channel {
			isLogChannel = true
			return
		}
	}
	return
}

func (l *Logger) removeOldLogs(p string, f os.FileInfo, err error) (e error) {
	filename := path.Base(p)
	if filename == "." || filename == "/" || !strings.Contains(filename, ".log.txt") || strings.Contains(filename, "latest.log.txt") {
		return nil
	}

	if err != nil {
		e = err
		return
	}

	strdate := strings.TrimSuffix(filename, ".log.txt")
	strdate = strings.Split(strdate, "_")[1] // Second substring after _ should be the date
	t, e := time.Parse("2006-01-02", strdate)
	if e != nil {
		return
	}

	d := time.Since(t)

	if d > l.logKeepTime {
		log.Printf("Removing old log file: %s", filename)
		e = os.Remove(p) // Outdated log file, removing...
		return
	}

	return
}

func replaceBadWords(str string) string {
	filterWords := map[string]string{
		"neger":   "vitlök",
		"negrar":  "vitlökar",
		"niggers": "garlics",
		"niggah":  "garlicah",
		"negroid": "vitlökroid",
		"jude":    "smålänning",
		"judar":   "smålänningar",
		"penis":   "vagina",
		"fuck":    "knulla",
		"fucker":  "knullare",
		"kuk":     "fitt",
		"turk":    "svensk",
	}

	for oldWord, newWord := range filterWords {
		s := strings.ToLower(str)

		for {
			index := strings.Index(s, oldWord)

			if index < 0 {
				break
			}

			str = str[:index] + newWord + str[index+len(oldWord):]
			s = strings.ToLower(str)
		}
	}

	return str
}

func (l *Logger) writeLogMessage(from, nick, msg string) {
	from = strings.TrimPrefix(from, "#") // Remove # from channel name

	logtime := time.Now()
	filename := fmt.Sprintf("%s_%s.log.txt", from, logtime.Format("2006-01-02"))
	logcontent := fmt.Sprintf("(%s) <%s> %s\n", logtime.Format("15:04:05 MST"), nick, replaceBadWords(msg))
	filepath := path.Join(l.logDir, filename)
	latestpath := path.Join(l.logDir, fmt.Sprintf("%s_%s", from, "latest.log.txt"))

	if _, err := os.Stat(filepath); os.IsNotExist(err) {
		// Symlink to a "latest" filepath, good for always linking to the latest channel log
		os.Remove(latestpath)
		os.Symlink(filename, latestpath)
	}

	f, err := os.OpenFile(filepath, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0775)
	if err != nil {
		log.Println(err)
		return
	}
	defer f.Close() // Close on exit of function

	if _, err = f.WriteString(logcontent); err != nil {
		log.Println(err)
	}
}

func (l *Logger) logMaid(d time.Duration) {
	if d < (time.Duration(10) * time.Second) {
		return // Do not start log maid on too low interval
	}

	var err error
	for {
		// Remove old log files
		if err = filepath.Walk(l.logDir, l.removeOldLogs); err != nil {
			log.Println(err)
		}

		time.Sleep(d)
	}
}

func (l *Logger) eventListener(event *irc.Event) {
	go func(event *irc.Event) {

		from := event.Arguments[0]
		if !l.fromLogChannel(from) {
			return // Ignore messages not coming from the registered log channels
		}

		msg := event.Message()

		switch event.Code {
		case "CTCP_ACTION":
			l.writeLogMessage(from, "*", fmt.Sprintf("%s %s", event.Nick, msg)) // change format to: "(2016-07-11) <*> nickname msg"
		case "JOIN":
			l.writeLogMessage(from, "-", fmt.Sprintf("%s (%s) has joined %s", event.Nick, event.Host, from))
		case "QUIT":
			l.writeLogMessage(from, "-", fmt.Sprintf("%s (%s) has quit %s", event.Nick, event.Host, from))
		case "PART":
			l.writeLogMessage(from, "-", fmt.Sprintf("%s (%s) has left %s", event.Nick, event.Host, from))
		case "MODE":
			l.writeLogMessage(from, "-", fmt.Sprintf("Mode %s [%s %s] by %s (%s)", from, event.Arguments[1], msg, event.Nick, event.Host))
		default:
			l.writeLogMessage(from, event.Nick, msg)
		}

		return
	}(event)
}

// Setup ...
func (l *Logger) Setup(irc *irc.Connection) error {

	irc.AddCallback("PRIVMSG", l.eventListener)
	irc.AddCallback("CTCP_ACTION", l.eventListener)
	irc.AddCallback("JOIN", l.eventListener)
	irc.AddCallback("QUIT", l.eventListener)
	irc.AddCallback("PART", l.eventListener)
	irc.AddCallback("MODE", l.eventListener)

	go l.logMaid(time.Duration(24) * time.Hour) // Remove old files every 24 hour

	return nil
}

// Usage ...
func (l *Logger) Usage() string {
	return ""
}
