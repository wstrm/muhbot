package service

import (
	"log"
	"math/rand"
	"time"

	"github.com/thoj/go-ircevent"
)

// GoodMorning ...
type GoodMorning struct {
	ircService
	hour, minute             uint64   // Hour and minute to print message
	randomHour, randomMinute uint64   // Randomize between 0..<random>
	channels                 []string // Channels to send the good morning message
}

var gmMessages = []string{
	"God morgon HTS!",
	"God moooorgooooooooon!",
	"God morgon HTS!",
	"صباح الخير",
	"God morgon HTS!",
	"Guten Morgen!",
	"Good morning",
	"God morgon HTS!",
	"God morron",
	"God morgon, vilken härlig dag!",
	"God morgon HTS!",
	"God morgon, har alla sovit gott?",
	"God morgon HTS!",
	"Åh vad härlig morgon",
	"Mmmmm kaffe, god morgon allihopa",
	"God morgon HTS!",
	"Ööh, god morgon HTS!",
	"God morgon HTS!",
	"God morgon från oss på LTU!",
	"Guten Morgen",
	"God morgon HTS!",
	"Hej svejs lever pastej nu vart det morgon",
	"God morgon HTS!",
	"Morgen HTS!",
	"Morgon HTS!",
	"Morrn morrn, har allesammans sovit gott?",
	"Morrn morrn",
	"Morrn morrn!",
	"God morgon HTS!",
	"Föredrar att sova än att vara vaken, meeen god morgon HTS",
	"God morgon HTS!",
	"GOD MORGON HTS",
	"GOD MORGON HTS!",
	"Guten Tag, öööh, Morgen HTS!",
}

func randomMessage(messages []string) string {
	msgLen := len(messages)
	if msgLen < 1 {
		log.Fatal("invalid length of messages, atleast one has to be defined")
	}

	randIndex := rand.Intn(msgLen - 1)

	return messages[randIndex]
}

// SetTime ...
func (g *GoodMorning) SetTime(t string) {
	h := int((t[0]-'0')*10 + (t[1] - '0'))
	m := int((t[3]-'0')*10 + (t[4] - '0'))
	if h < 0 || h > 23 || m < 0 || m > 59 {
		log.Fatal("Invalid time format")
	}

	g.hour = uint64(h)
	g.minute = uint64(m)
}

// SetChannels ...
func (g *GoodMorning) SetChannels(c []string) {
	g.channels = c
}

// SetRandom ...
func (g *GoodMorning) SetRandom(randomHour, randomMinute uint64) {
	g.randomHour, g.randomMinute = randomHour, randomMinute
}

// Setup ...
func (g *GoodMorning) Setup(conn *irc.Connection) error {
	go func() {
		for {
			now := time.Now()
			hour := rand.Intn(int(g.randomHour)) + int(g.hour)
			minute := rand.Intn(int(g.randomMinute)) + int(g.minute)
			second := rand.Intn(59)
			nsecond := rand.Intn(59)
			day := now.Day() + 1 // Increment day by one, or else we may get a negative duration - infinite loop
			month := now.Month()
			year := now.Year()

			nextTime := time.Date(year, month, day, hour, minute, second, nsecond, time.Local)
			waitDuration := nextTime.Sub(now)

			log.Printf("GoodMorning: will post message at %s (in %s)", nextTime.String(), waitDuration.String())

			time.Sleep(waitDuration)
			for _, channel := range g.channels {
				conn.Privmsg(channel, randomMessage(gmMessages))
			}
		}
	}()

	return nil
}

// Usage ...
func (g *GoodMorning) Usage() string {
	return ""
}
