package main

import (
	"flag"
	"log"
)

type channelList []string

type flags struct {
	address     string
	nick        string
	user        string
	owner       string
	password    string
	logDir      string
	channels    channelList
	tls         bool
	tlsNoVerify bool
	debug       bool
}

func (c channelList) List() (channels []string) {
	for _, channel := range c {
		channels = append(channels, channel)
	}
	return
}

func (c *channelList) String() (str string) {
	for _, channel := range c.List() {
		str += channel + " "
	}
	if len(str) > 1 {
		str = str[:(len(str) - 1)] // Trim whitespace at end
	}
	return
}

func (c *channelList) Set(channel string) error {
	*c = append(*c, channel)
	return nil
}

var context = flags{
	owner:  "unknown",
	nick:   "muhbot",
	user:   "muhbot",
	logDir: "./",
}

func init() {
	flag.StringVar(&context.owner, "owner", context.owner, "Owner for bot.")
	flag.StringVar(&context.nick, "nick", context.nick, "Nick for bot.")
	flag.StringVar(&context.user, "user", context.user, "Username for bot.")
	flag.StringVar(&context.address, "address", context.address, "IRC server address (with port).")
	flag.StringVar(&context.password, "password", context.password, "Password for authenticating to IRC server.")
	flag.StringVar(&context.logDir, "log-dir", context.logDir, "Directory to store logs in.")
	flag.Var(&context.channels, "channel", "Channel to join, use flag repeatedly for multiple channels.")
	flag.BoolVar(&context.tls, "tls", context.tls, "Use TLS.")
	flag.BoolVar(&context.tlsNoVerify, "tls-no-verify", context.tlsNoVerify, "Do not verify TLS certificate.")
	flag.BoolVar(&context.debug, "debug", context.debug, "Enable more verbose logging.")

	flag.Parse()

	if !context.tls && context.tlsNoVerify {
		log.Fatal("-tls-no-verify requires -tls flag")
	}

	if context.address == "" {
		log.Fatal("-address flag is required")
	}

	if len(context.channels.List()) < 1 {
		log.Fatal("atleast one -channel flag is required")
	}
}
